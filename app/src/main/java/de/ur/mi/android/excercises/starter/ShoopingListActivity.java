package de.ur.mi.android.excercises.starter;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;


/**
 * Created by Luci on 04.09.2016.
 */
public class ShoopingListActivity extends Activity{

    private Button addShoppingList;
    private ListView shoppingLists;

    @Override
    protected void onCreate(Bundle savedInstanceService){
        super.onCreate(savedInstanceService);
        setContentView(R.layout.shopping_lists);
        initUI();
        initListener();
    }



    private void initUI() {
        addShoppingList = (Button) findViewById(R.id.add_shopping_list);
        shoppingLists = (ListView) findViewById(R.id.shopping_list);
    }

    private void initListener() {
        addShoppingList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), ShoppingListItemActivity.class);
                startActivity(i);
            }
        });
    }
}
