package de.ur.mi.android.excercises.starter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.firebase.client.Firebase;

/**
 * Created by Katha on 31.08.2016.
 */
public class EnterOrCreateWgActivity extends Activity implements View.OnClickListener {

    private EditText enterID;
    private Button enterExistingWgButton, createNewWgButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.enter_or_create_new_wg_layout);
        Firebase.setAndroidContext(this);
        initAlertDialog();
        initUI();

    }

    private void initAlertDialog() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(EnterOrCreateWgActivity.this);
        dialog.setTitle(R.string.dialog_welcome);
        dialog.setMessage(R.string.dialog_welcome_Text);
        dialog.setPositiveButton(R.string.dialog_ok, null);
        dialog.show();
    }

    private void initUI() {
        enterID = (EditText) findViewById(R.id.enter_extisting_wg_edit_id);
        enterExistingWgButton = (Button) findViewById(R.id.enter_extisting_wg_button_id);
        createNewWgButton = (Button) findViewById(R.id.create_new_wg_button_id);
        enterExistingWgButton.setOnClickListener(this);
        createNewWgButton.setOnClickListener(this);
    }

    private String getExtras(){
        String registerdPersonId = getIntent().getExtras().getString("id");
        return registerdPersonId;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.enter_extisting_wg_button_id: {
                String enterIDString = enterID.getText().toString();
                //enterIDString mit Datenbank abgleichen und bei übereinstimmung per Intent die MainMenueActivity starten

            }
            case R.id.create_new_wg_button_id: {
                Intent i = new Intent(getApplicationContext(), CreateNewWgActivity.class);
                String id = getExtras();
                i.putExtra("id", id);
                startActivity(i);
            }
        }
    }
}
