package de.ur.mi.android.excercises.starter;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.firebase.client.Firebase;

/**
 * Created by Katha on 01.09.2016.
 */
public class AddAndShowWgFriendsActivity
        extends Activity {

    private TextView wgIdTextView;
    private Button inviteFriendsButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_menue);
        Firebase.setAndroidContext(this);
        initUI();
        initListener();


    }

    private void initUI(){
        wgIdTextView = (TextView) findViewById(R.id.wg_id_label);
        wgIdTextView.setText(getText(R.string.wg_id )+ ": " + wgIdString());
        inviteFriendsButton =(Button) findViewById(R.id.invite_friends_button_id);

    }


    private void initListener(){
        inviteFriendsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String shareBody = "Hey du wurdest von mir in meine WG eingeladen! Gibt diese ID an: \n" + wgIdString();
                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Subject Here");
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
                startActivity(Intent.createChooser(sharingIntent, getResources().getString(R.string.wg_id)));
            }
        });
    }

    private String wgIdString(){
        String wgId = getIntent().getExtras().getString("id");
        return wgId;

    }
}
