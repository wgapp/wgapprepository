package de.ur.mi.android.excercises.starter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.firebase.client.ChildEventListener;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.Query;
import com.firebase.client.ValueEventListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;


public class MainActivity extends Activity implements View.OnClickListener {

   private Firebase mRootRef;
   private EditText eMail, password;
   private Button login, registry, test;

   @Override
   protected void onCreate(Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);
      setContentView(R.layout.activity_main);
      initUI();
      initFirebase();



   }
   private void initFirebase(){

   }

   private void initUI(){
      test = (Button) findViewById(R.id.test);// Test für neue Activitys

      eMail = (EditText) findViewById(R.id.e_mail_edit);
      password = (EditText)findViewById(R.id.password_edit);
      login = (Button)findViewById(R.id.log_in_button_id);
      registry = (Button)findViewById(R.id.registry_button_id);
      login.setOnClickListener(this);
      registry.setOnClickListener(this);
      test.setOnClickListener(this);
      Firebase.setAndroidContext(this);
      mRootRef = new Firebase("https://wg-app-61181.firebaseio.com/RegisterPerson");
   }
   @Override
   public boolean onCreateOptionsMenu(Menu menu) {

      // Inflate the menu; this adds items to the action bar if it is present.
      getMenuInflater().inflate(R.menu.main, menu);
      return true;
   }

   private void alertDialog(){
      AlertDialog.Builder dialog = new AlertDialog.Builder(MainActivity.this);
      dialog.setTitle(R.string.dialog_log_in_title);
      dialog.setMessage(R.string.dialog_log_in_title);
      dialog.setPositiveButton(R.string.dialog_ok, null);
      dialog.show();
   }


   @Override
   public void onClick(View view) {
      switch (view.getId()){
         case R.id.test:{
            Intent x = new Intent(getApplicationContext(), ShoopingListActivity.class);
            startActivity(x);
            break;
         }
         case R.id.log_in_button_id: {

            //überprüfen von name und passwort

             final String eMailAddressString = eMail.getText().toString();
             final String passwordString = password.getText().toString();
            if(eMailAddressString.equals("")|| passwordString.equals("")){
               alertDialog();
            }
            final Firebase emailRef = mRootRef.child("email");
            Firebase passwordRef = mRootRef.child("password");

            Query query = mRootRef.orderByChild("email").equalTo(eMailAddressString);
            if(query == null){
              alertDialog();

            }
            query.addListenerForSingleValueEvent(new ValueEventListener() {
               @Override
               public void onDataChange(DataSnapshot dataSnapshot) {
                  System.out.println();
                  for(DataSnapshot snapshot: dataSnapshot.getChildren()) {
                     String name = snapshot.child("name").getValue(String.class);
                     String email = snapshot.child("email").getValue(String.class);
                     String password = snapshot.child("password").getValue(String.class);
                     System.out.println(name + " " + email + " " + password);
                     System.out.println("EMail" + email);
                     if(password.equals(passwordString)){
                        Intent i = new Intent(getApplicationContext(), AddAndShowWgFriendsActivity.class);
                        startActivity(i);

                     }


                     else{
                        alertDialog();
                     }
                  }
               }

               @Override
               public void onCancelled(FirebaseError firebaseError) {

               }
            });
            Query queryPassword = mRootRef.orderByChild("password").equalTo(passwordString);
            System.out.println(queryPassword);
            if(queryPassword == null){
               alertDialog();
            }
            queryPassword.addListenerForSingleValueEvent(new ValueEventListener() {
               @Override
               public void onDataChange(DataSnapshot dataSnapshot) {

                  for(DataSnapshot snapshot: dataSnapshot.getChildren()) {
                     String name = snapshot.child("name").getValue(String.class);
                     String email = snapshot.child("email").getValue(String.class);
                     String password = snapshot.child("password").getValue(String.class);
                     System.out.println(name + " " + email + " " + password);
                     System.out.println("Passwort" + password);
                     if (email.equals(eMailAddressString)) {
                        Intent i = new Intent(getApplicationContext(), AddAndShowWgFriendsActivity.class);
                        startActivity(i);

                     } else {
                         alertDialog();
                     }
                  }

               }

               @Override
               public void onCancelled(FirebaseError firebaseError) {
               }
            });


         }

         case R.id.registry_button_id:{
            Intent i = new Intent(getApplicationContext(), RegistryActivity.class);
            startActivity(i);
         }


      }

   }

}
