package de.ur.mi.android.excercises.starter;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.firebase.client.Firebase;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Katha on 31.08.2016.
 */
public class CreateNewWgActivity extends Activity {
    private EditText wgName, wgAddress;
    private Button createWg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.create_new_wg_layout);
        Firebase.setAndroidContext(this);
        initUI();
        initListener();

    }

    private void initUI(){
        wgName = (EditText) findViewById(R.id.wg_name_edit_id);
        wgAddress = (EditText) findViewById(R.id.wg_address_edit_id);
        createWg = (Button) findViewById(R.id.create_wg_button_id);
    }

    private void initListener(){
        createWg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String wgNameString = wgName.getText().toString();
                String wgAddressString = wgAddress.getText().toString();
                String idRoot = getIntent().getExtras().getString("id");
                final Firebase firebase = new Firebase("https://wg-app-61181.firebaseio.com/RegisterPerson/" + idRoot+"/").child("Wg");
                Map<String, String> wg = new HashMap<String, String>();
                wg.put("wgName", wgNameString);
                wg.put("wgAddress", wgAddressString);
                Firebase myfirebaseRef = firebase.push();
                myfirebaseRef.setValue(wg);
                //value gibt den automatisch generierten Key einer registrierten Person zurück. Bitte löscht die Log.d dinge erst aus dem code, wenn wir das Projekt abgeben :D
                String value = myfirebaseRef.getKey();
                Log.d("IDWERT", value);

                Intent i = new Intent(getApplicationContext(), AddAndShowWgFriendsActivity.class);
                i.putExtra("id", value);
                startActivity(i);

            }
        });
    }
}
