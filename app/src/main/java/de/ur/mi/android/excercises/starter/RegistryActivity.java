package de.ur.mi.android.excercises.starter;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Katha on 22.08.2016.
 */
public class RegistryActivity extends Activity {
    private EditText name, email, password;
    private Button registry, test;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.registry_layout);
         Firebase.setAndroidContext(this);
        initUI();
        initButton();




}

    private void initUI(){
        registry = (Button) findViewById(R.id.registry_button_id);
        name = (EditText) findViewById(R.id.name_edit);
        email = (EditText) findViewById(R.id.e_mail_edit);
        password = (EditText) findViewById(R.id.password_edit);
    }

    private void initButton(){
        registry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String emailString = email.getText().toString();
                String nameString = name.getText().toString();
                String passwordString = password.getText().toString();
                final Firebase firebase = new Firebase("https://wg-app-61181.firebaseio.com/").child("RegisterPerson");
                Map<String, String> registerPerson = new HashMap<String, String>();
                registerPerson.put("name", nameString);
                registerPerson.put("email", emailString);
                registerPerson.put("password", passwordString);
                Firebase myfirebaseRef = firebase.push();
                myfirebaseRef.setValue(registerPerson);
                //value gibt den automatisch generierten Key einer registrierten Person zurück. Bitte löscht die Log.d dinge erst aus dem code, wenn wir das Projekt abgeben :D
                String value = myfirebaseRef.getKey();
                Log.d("IDWERT", value);

                Toast.makeText(getApplicationContext(), R.string.successfully_registered, Toast.LENGTH_SHORT).show();
                Intent i = new Intent(getApplicationContext(), EnterOrCreateWgActivity.class);
                i.putExtra("id", value);
                startActivity(i);
            }
        });
    }
}
