package de.ur.mi.android.excercises.starter;

/**
 * Created by Katha on 30.08.2016.
 */
public class RegisterPerson {

    //name and address string
    private String name;
    private String email;
    private String password;

    public RegisterPerson() {
      /*Blank default constructor essential for Firebase*/
    }
    //Getters and setters
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword(){
        return password;
    }
    public void setPassword(String password){
        this.password = password;
    }

    public RegisterPerson(String name, String email, String password) {
      /*Blank default constructor essential for Firebase*/
        this.name = name;
        this.email = email;
        this.password = password;
    }
    public RegisterPerson(String name, String password) {
        this.name = name;
        this.password = password;
      /*Blank default constructor essential for Firebase*/
    }
}
