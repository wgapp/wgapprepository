package de.ur.mi.android.excercises.starter;

/**
 * Created by Katha on 31.08.2016.
 */
public class Wg {
    private String wgName;
    private String wgAddress;


    public Wg() {
      /*Blank default constructor essential for Firebase*/
    }
    //Getters and setters
    public String getWgName() {
        return wgName;
    }

    public void setWgName(String wgName) {
        this.wgName = wgName;
    }

    public String getWgAddress() {
        return wgAddress;
    }

    public void setWgAddress(String wgAddress) {
        this.wgAddress = wgAddress;
    }
    public Wg(String wgName, String wgAddress){
        this.wgName = wgName;
        this.wgAddress = wgAddress;
    }
}
